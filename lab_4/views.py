from django.http import response
from django.shortcuts import render

# Import Note models from Lab-2 yang akan digunakan untuk fungsi index
from lab_2.models import Note

#Import HttpResponseRedirect dan NoteForm untuk fungsi add-note
from django.http.response import HttpResponseRedirect
from .forms import NoteForm

# Index method
def index(request):
    modelNotes = Note.objects.all().values()
    response = {'modelNotes': modelNotes}
    return render(request, 'lab4_index', response)

# Method untuk menambahkan note
def add_note(request):
    context = {}
    form = NoteForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        form.save() 
        return HttpResponseRedirect('/lab-4')
    context['form'] = form
    return render(request, 'lab4_form', context)

# Method untuk menampilkan note sebagai kartu
def note_list(request):
    allnotes = Note.objects.all()
    response = {'allnotes' : allnotes}
    return render(request, 'lab4_note_list', response)