# Import model Note yang telah dibuat di lab 2 karena akan digunakan disini
from django import forms
from lab_2.models import Note

# Buat kelas untuk NoteForm menggunakan model Note
# Pastikan semua fields telah digunakan
class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['To', 'From', 'Title', 'Message']