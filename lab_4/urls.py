# Import semua fungsi yang akan digunakan untuk meng-handle semua link
from django.urls import path
from .views import index, add_note, note_list

# Tambahkan url pattern untuk lab-4/, lab-4/add-note, dan lab-4/note-list
urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_note, name='add_note'),
    path('note-list', note_list, name='note_list')
]