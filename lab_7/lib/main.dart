// Lab 7 PBP : Flutter Input and Form (--all to do checklist in instruction has been done--)

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    // Atur nama aplikasi
    final appTitle = 'BIMBOL';

    // Material app dan atur tema
    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        primarySwatch: Colors.purple,
        fontFamily: 'Raleway',
      ),

      // Scaffold dengan appbar yang sudah di set dan body custom form
      home: Scaffold(
        appBar: AppBar(
          title: const Text(
            'BIMBOL\nBimbel di Masa Pandemi',
            textAlign: TextAlign.center,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
          centerTitle: true,
        ),
        backgroundColor: Colors.purple[50],
        body: MyCustomForm(),
      ),
    );
  }
}
// Buat custom form state
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Buat class
class MyCustomFormState extends State<MyCustomForm> {

  // Buat final yang dibutuhkan
  final _formKey = GlobalKey<FormState>();
  final nameHolder1 = TextEditingController();
  final nameHolder2 = TextEditingController();
  final nameHolder3 = TextEditingController();

  // Fungsi untuk menghapus input jika sebuah event dilakukan yaitu klik tombol
  clearTextInput(){
    nameHolder1.clear();
    nameHolder2.clear();
    nameHolder3.clear();
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            controller: nameHolder1,
            decoration: const InputDecoration(
              icon: const Icon(Icons.person),
              hintText: 'Siapa yang memberikan saran ini?',
              labelText: 'Nama',
            ),
          ),
          TextFormField(
            controller: nameHolder2,
            decoration: const InputDecoration(
              icon: const Icon(Icons.book),
              hintText: 'Saran ini tentang apa?',
              labelText: 'Judul Saran',
            ),
          ),
          TextFormField(
            controller: nameHolder3,
            decoration: const InputDecoration(
              icon: const Icon(Icons.aod_sharp),
              hintText: 'Berikan saran anda!',
              labelText: 'Saran',
            ),
          ),
          new Container(
              padding: const EdgeInsets.only(left: 150.0, top: 40.0),
              child: new ElevatedButton(
                child: const Text('Submit'),
                onPressed: clearTextInput,
              )),
        ],
      ),
    );
  }
}

