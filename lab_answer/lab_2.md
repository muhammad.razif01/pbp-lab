### **Jawaban Pertanyaan untuk Lab 2 PBP**

##### **1. Apakah perbedaan antara JSON dan XML?**

Perbedaan utama diantara JSON dan XML terletak pada **fungsi atau tujuan penggunaan dari keduanya.**
> JSON digunakan oleh programmer untuk **merepresentasikan objek/data dalam bentuk yang bisa dengan mudah dibaca manusia** dan biasa digunakan untuk *data-interchange* (oleh karena itu namanya JavaScript OBJECT NOTATION)
>
> XML adalah **markup language** yang biasa digunakan untuk formatting dengan **menambahkan informasi tambahan ke teks biasa** (oleh karena itu namanya Extensible MARKUP LANGUAGE)

Selain perbedaan utama di atas, terdapat perbedaan-perbedaan lain seperti:
+ XML diturunkan dari SGML sedangkan JSON berdasar pada JavaScript
+ XML punya **start tag** dan **end tag** sedangkan JSON tidak punya **end tag**
+ XML *support* **Namespaces** sedangkan JSON tidak
+ XML *support* **Comments** sedangkan JSON tidak
+ XML *support* berbagai tipe encoding sedangkan JSON hanya *support* **UTF-8 Encoding**
+ JSON *support* **Array** sedangkan JSON tidak
+ JSON cenderung lebih *secure* dibandingkan XML
+ JSON cenderung lebih mudah dibaca dan dipahami manusia dibandingkan XML
+ JSON cenderung memiliki ukuran file yang lebih kecil dibandingkan XML

Selain perbedaan-perbedaan di atas, terdapat satu lagi perbedaan yang sangat signifikan, yaitu data JSON disimpan dalam bentuk seperti **map (key:value)** sedangkan data XML disimpan dalam bentuk *tree*. 

##### **2. Apakah perbedaan antara HTML dan XML?**

Secara fundamental, keduanya berbeda.
> HTML adalah **markup language sederhana** dan **tidak membawa data**
>
> XML adalah markup language yang **memberikan kerangka mendefinisikan markup language lain** dan **membawa data dari atau ke database**

Terdapat perbedaan pada **fokus** dan **apa yang didefinisikan** oleh keduanya. Perbedaan tersebut adalah: 
> HTML berfokus pada bagaimana **data terlihat dan ditampilkan**. Oleh karena itu, HTML mendefinisikan **struktur dan tampilan dari web sekaligus informasi yang ditampilkannya**.
>
> XML berfokus pada men-transfer, menyimpan, dan menstruktur data. Oleh karena itu, XML mendefinisikan **bagaimana sebuah data distruktur dan disimpan serta di-transfer**.

Beberapa perbedaan lain dari keduanya adalah:
+ XML **case-sensitive** sedangkan HTML tidak
+ XML wajib menggunakan **closing-tag** sedangkan pada HTML bukanlah hal yang wajib
+ XML tidak **tolerate error** sedangkan HTML bisa mengabaikan *small error*
+ XML bisa menyimpan **white-spaces** sedangkan HTML tidak
+ XML cenderung **dinamis** dan HTML cenderung **statis**
+ Tag pada XML digunakan untuk **describing data** sedangkan pada HTML digunakan untuk **displaying data**
+ Tag pada XML bersifat **fleksibel dan didefinisikan sendiri oleh programmer** sesuai kebutuhan sedangkan tag pada HTML sudah didefinisikan standard nya.
+ XML butuh aplikasi **XML DOM** dan pengimplementasian kode untuk *map-back* objek kembali menjadi objek JavaScripts sedangkan HTML tidak butuh kode tambahan untuk **parse text**

Sumber atau referensi yang digunakan:
+ *https://www.geeksforgeeks.org/difference-between-json-and-xml/*
+ *http://www.differencebetween.net/technology/protocols-formats/difference-between-json-and-xml/*
+ *https://www.upgrad.com/blog/html-vs-xml/*
+ *https://www.geeksforgeeks.org/html-vs-xml/*