from django.http.response import HttpResponse
from django.core import serializers

from django.shortcuts import render
from .models import Note


# Create your views here.

#ToDo : Implement views.py

def index(request):
    modelNotes = Note.objects.all().values()
    #'modelNote' in template : 'modelNote' in views
    response = {'modelNotes': modelNotes}
    return render(request, 'lab2', response)

def xml(request):
    modelNotes = Note.objects.all()
    data = serializers.serialize('xml', modelNotes)
    return HttpResponse(data, content_type="application/xml")

def json(request):
    modelNotes = Note.objects.all()
    data = serializers.serialize('json', modelNotes)
    return HttpResponse(data, content_type="application/json")
