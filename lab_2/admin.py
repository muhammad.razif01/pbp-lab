from django.contrib import admin

# Register your models here.

#ToDo : Register 'Note' model so it can be accessed through Django Admin
from .models import Note
admin.site.register(Note)