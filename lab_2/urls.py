from django.urls import path
from .views import index, xml, json

#ToDo : Create urlpatterns

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('json', json, name='json')
]