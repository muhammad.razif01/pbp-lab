# Generated by Django 3.2.7 on 2021-09-26 08:35

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('instanceTo', models.CharField(max_length=100)),
                ('instanceFrom', models.CharField(max_length=100)),
                ('instanceTitle', models.CharField(max_length=100)),
                ('instanceMessage', models.TextField()),
            ],
        ),
    ]
